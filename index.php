<?php
/**
 * Script to get Employee payout Data from Airtable and 
 * Post it to client server with basic Auth
 * 
 * Example Airtable Base(Public): https://airtable.com/shrp9CK5JLPzyhU2z
 *
 * PHP version 8.0.3
 * 
 * Libraries used: 
 * https://github.com/sleiman/airtable-php
 * https://github.com/guzzle/guzzle
 *
 * LICENSE: N/A
 *
 * @author     Original Author gyana111@gmail.com
 * @copyright  N/A
 * @version    1.0
 */

require 'vendor/autoload.php';

use \TANIOS\Airtable\Airtable;
$airtable = new Airtable(array(
    'api_key' => 'keyVvR4Xse2xEiAVz',
    'base'    => 'app33ofeGVzDDgbkz'
));

$params = array(
    "sort" => array(array('field' => 'Pay Date', 'direction' => "asc")),
    "fields" => array('Employee Name', 'Amount'),
);

$request = $airtable->getContent('Payouts', $params);

$allPayouts = array();

do {
    $response = $request->getResponse();
    $allPayouts = array_merge($allPayouts,$response[ 'records' ]);
}
while( $request = $response->next() );

// arrange the payouts as an array with names as key and total payment as values
$totalPayments = array();
foreach ($allPayouts as $payout) {

    if(isset($totalPayments[$payout->fields->{'Employee Name'}])) {
        $totalPayments[$payout->fields->{'Employee Name'}] +=  (float)$payout->fields->Amount;
    } else {
        $totalPayments += array($payout->fields->{'Employee Name'} => (float)$payout->fields->Amount);
    }
}

// convert the float amount to dollar amount rounded to it's nearest cent value
$totalPayments = array_map(function($value) { return round($value, 2); }, $totalPayments);

// convert the payments to json for sending with the post payload
$data = json_encode($totalPayments);

// send the data with post request
$client = new GuzzleHttp\Client();

$response = $client->post('https://auth.da-dev.us/devtest1', [
    'auth'    => [
        'gyana111@gmail.com', 
        '4P#Wle5h4*'
    ],
    'json'    => [
        $totalPayments
    ]
]);

echo $response->getBody();